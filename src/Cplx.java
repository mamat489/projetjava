
public interface Cplx {
	
	public double getReel();
	public double getIm();
	public double getModule();
	public double getArgument();
	public void ajoute(Cplx complexe);
	public void multiplication(Cplx complexe);
	public void soustrait(Cplx complexe);

}
